/** @file solution1.c
    @brief Sending Heartbeat message to AWS ioT core cloud using MQTT protocol

       *  Heartbeat messages are those messages that are sent to the cloud to say that the device is alive.
       *  This code fetches heartbeat message in JSON format.
       *  And sends it to AWS ioT core cloud through MQTT protocol.
       *  User can see this message by simply subscribing to a particular topic.
       
    @JananiB
*/




/** Standard includes*/
#include <stdio.h> // for Standard Input Output
#include <stdlib.h> // for memory allocation, process control,conversions and others
#include <string.h> // for manipulating C strings and arrays

/** Network Header files */
/** we will use this header files
    to fetch IP address of the device */
#include <netdb.h> // for network database operations.
#include <ifaddrs.h> // for getting network interface addresses.

/** Shunya interfaces header file*/
/** We will use this header file to send our JSON
    message to the AWS cloud using Shunya interfaces APIs */
#include <shunyaInterfaces.h> // to program embedded boards to talk to PLC's, Sensors, Actuators and Cloud

/** Time Header file*/
/** We will use this header file to fetch timestamp */
#include <time.h> // to get and manipulate date and time information

/** MQTT protocol Header file*/
/** We will use this header file to make MQTT message payload*/
#include "MQTTClient.h" // contains APIs to send/receive MQTT data



/** @brief devicedata

    * This function will fetch the data of the device
    * This data contains parameters which will be stored in JSON format
    * This JSON message contains id of device, timestamp, type of message,
      and an IP address of the network connected.
    * first id of the device which is stored in "/etc/shunya/deviceid" will be fetched.
    * Then UNIX timestamp fetched through API from time.h header file.
    * After that type of event which will be "heartbeat" message.
    * At the end we will fetch an IP address of the network at which our device is connected

    
*/
void devicedata(void)
{
// Fetches ID of the device
FILE *idl;
char id[255];
idl = fopen("/etc/shunya/deviceid", "r");
while(fscanf(idl, "%s", id)!=EOF)
{printf("%s ", id );
}

// Fetches timestamp
int stp = (gettimeofday(2));

// Fetches IP address
void checkIPbuffer(char *ipadd)
{
if (NULL == ipadd)
{
perror("inet_ntoa");
exit(1);
}
}
char *ipadd;
ipadd = inet_ntoa(*((struct in_addr*)
host_entry->h_addr_list[0]));}



void AWS_config(void)
{
"user":
 {
"endpoint": " ",
"port": 8883,
"certificate dir": "/home/shunya/.cert/aws/",
"root certificate": " ",
"client certificate": " ",
"private key": " ",
"client ID": " "
}
}


/** @brief send_data

    * This function will send a JSON message to an AWS ioT core MQTT broker.
   
*/
void send_data()
{MQTTClient_message heartbeat = MQTTClient_message_initializer; //initializes message to be sent on MQTT broker
awsObj user = newAws("user"); //creates new instance with AWS
awsConnectMqtt(&user); //connects to the MQTT broker of AWS ioT core
heartbeat.payload("device":{
"deviceId": id,
"timestamp": stp,
"eventType": "heartbeat",
"ipAddress": ipadd
}.getBytes()); //creates a payload of the message
awsPublishMqtt(&user, "device/heartbeat", "%s" ,heartbeat); //publishes a payload on AWS MQTT broker
awsDisconnectMqtt(&user); //releases connection with MQTT broker
}

/** @brief main function

    * This function will call all the functions made above and completes whole process.
    
*/
int main (void)
{
shunyaInterfacesSetup () ;// function to initiate shunya interfaces
devicedata(); // function to get data of the device (device id, timestamp, event type, IP address
AWS_config(); // function to configure connection with AWS MQTT broker
send_data(); // function to publish data on AWS MQTT broker
}

