/*
	*  Code for reading data from the RISHI Meter 3430 using Shunya Interfaces modbus API's.
	*  Current sensing, voltage sensing and power sensing units of the meter will give us data for required parameters.
	*  Fetching data for the required parameters from these units using an IoT Gateway and MODBUS (RS485) interface.
*/
#include <stdio.h> // for Standard Input and Output
#include <stdlib.h> // for functions involving memory allocation, process control,conversions and others
#include <string.h> // for manipulating C strings and arrays
#include <shunyaInterfaces.h> // to program embedded boards to talk to PLC's, Sensors, Actuators and Cloud
#include <dirent.h> // format of directory entries

x= opendir("/dev/ttyAMA0");
int main(void) //main function
{
shunyaInterfacesSetup();

//modbus TCP/IP configuration
//user enter their modbus data
"modbus-tcp":{
"type": "tcp",
"ipaddr": " ",
"port": " "}


//modbus connect read/write data
modbusObj x = newModbus("modbus-tcp"); // for creating new instance
modbusTcpConnect(&x); // connect to modbus as per configs

// reading data of different parameters
float ac_current = modbusTcpRead(&x, 30007); //ac-current
float voltage = modbusTcpRead(&x, 30001);// voltage
float power = modbusTcpRead(&x, 30013);// power
float Active_energy = modbusTcpRead(&x, 30147);//active_energy
float Reactive_energy = modbusTcpRead(&x, 30151);//reactive energy
float Apparent_energy = modbusTcpRead(&x, 30081);// apparent energy
float THD_of_Voltage = modbusTcpRead(&x, 30219);// thd of voltage

return 0;
}
