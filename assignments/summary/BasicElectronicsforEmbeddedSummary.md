### EMBEDDED SYSTEMS
An embedded system is a microprocessor-based computer hardware system with software that is designed to perform a dedicated function, either as an independent system or as a part of a large system. At the core is an integrated circuit designed to carry out computation for real-time operations.The systems can be programmable or with fixed functionality. The complexity of an embedded system varies significantly depending on the task for which it is designed. Typically, IoT devices are embedded systems. In IoT, the devices which have been embedded with electronics, internet connectivity and other forms of hardware like sensors can communicate and interact with other devices over the internet.

![embedded system](https://www.eletimes.com/wp-content/uploads/2019/12/Embedded-Systems-booost.jpg)

#### Below are the basic electronic devices used in embedded systems:
* **SENSORS AND ACTUATORS**  
The intelligence and value from an IoT system is based on what can be learned from the data. Sensors are the source of IoT data. A better term for a sensor is a transducer. A transducer is any physical device that converts one form of energy into another. So, in the case of a sensor, the transducer converts some physical phenomenon into an electrical impulse that can then be interpreted to determine a reading.
Another type of transducer that encounters in many IoT systems is an actuator. In simple terms, an actuator operates in the reverse direction of a sensor. It takes an electrical input and turns it into physical action.
![sensors](https://bridgera.com//wp-content/uploads/2017/06/sensor_actuator_graphicssec1_pg16.jpg)

* **ANALOG AND DIGITAL**  
Analog and digital signals are different types which are mainly used to carry the data from one apparatus to another. Analog signals are continuous wave signals that change with time period whereas digital is a discrete signal is a nature. The main difference between analog and digital signals is, analog signals are represented with the sine waves whereas digital signals are represented with square waves. Let us discuss some dissimilarity of analog & digital signals.
![signals](https://1.bp.blogspot.com/-BOJ9ZDChtnQ/Xcw3rI-uRII/AAAAAAAACUM/lNFGx00PNucfiRQewbFUqjhz8g_cY2ingCLcBGAsYHQ/s1600/Difference%2Bbetween%2BAnalog%2Band%2BDigital%2BSignal.png)

* **MICROPROCESSORS AND MICROCONTROLLERS**  
An integrated circuit contained on a single silicon chip, a microprocessor contains the arithmetic logic unit, control unit, internal memory registers, and other vital circuitry of a computer's central processing unit (CPU). Microprocessor commonly is used interchangeably with CPU and processor.
A microcontroller is a compact integrated circuit designed to govern a specific operation in an embedded system. It is an integrated circuit (IC) device used for controlling other portions of an electronic system, usually via a microprocessor unit (MPU), memory, and some peripherals.  
![mc and mp](https://eeeproject.com/wp-content/uploads/2017/10/Microprocessor-vs-Microcontroller.jpg)

* **RASPBERRY Pi** 
Raspberry Pi is the name of a series of single-board computers made by the Raspberry Pi Foundation, a UK charity that aims to educate people in computing and create easier access to computing education. It is a low cost, credit-card sized computer that plugs into a computer monitor or TV, and uses a standard keyboard and mouse.  
![rp](https://upload.wikimedia.org/wikipedia/commons/f/f1/Raspberry_Pi_4_Model_B_-_Side.jpg)

* **PARALLEL AND SERIAL COMMUNICATION**  
![ps](https://www.altestore.com/howto/images/article/battery_series_parallel_examples.jpg)
