## WHAT IS ELECTRICAL SAFETY  
Electrical safety is a general practice of workers who are exposed to handling and maintaining electrically powered equipment. It is a set of guidelines they follow to mitigate electrical hazards and prevent its dangerous effects in case of an incident. Unable to adhere to electrical safety can lead to accidents, near misses, or even fatalities.

### POWER SUPPLY
Do’s: Always make sure that the output voltage of the power supply matches the input voltage of the board.
While turning on the power supply make sure every circuit is connected properly.

![ps](https://www.hydroquebec.com/themes/securite/images/maison/situations-dangereuses/troisieme-broche.png)

Don’ts: Do not connect power supply without matching the power rating.
Never connect a higher output(12V/3Amp) to a lower (5V/2Amp) input .
Do not try to force the connector into the power socket ,it may damage the connector.

### HANDLING
Do’s:   
* Treat every device like it is energized, even if it does not look like it is plugged in or operational.
* While working keep the board on a flat stable surface (wooden table) .
* Unplug the device before performing any operation on them.
* When handling electrical equipment, make sure your hands are dry.
* Keep all electrical circuit contact points enclosed.
* If the board becomes too hot try to cool it with a external usb fan .

![hn](https://edgeup.asus.com/wp-content/uploads/2015/08/Number-1-3.jpg)

Don’ts:  
* Don’t handle the board when its powered ON.
* Never touch electrical equipment when any part of your body is wet, (that includes fair amounts of perspiration).
* Do not touch any sort of metal to the development board.

### GPIO
Do’s:  
* Find out whether the board runs on 3.3v or 5v logic.
* Always connect the LED (or sensors) using appropriate resistors .
* To Use 5V peripherals with 3.3V we require a logic level converter.

![gpio](https://static.makeuseof.com/wp-content/uploads/2018/05/GPIO-pinout_670.jpg)

Don’ts:  
* Never connect anything greater that 5v to a 3.3v pin.
* Avoid making connections when the board is running.
* Don't plug anything with a high (or negative) voltage.
* Do not connect a motor directly, use a transistor to drive it .

## GUIDELINES FOR USING INTERFACES
1. UART  
A Universal Asynchronous Receiver-Transmitter is a computer hardware device for asynchronous serial communication in which the data format and transmission speeds are configurable.

![uart](https://microcontrollerslab.com/wp-content/uploads/2019/10/UART-BUS-between-two-devices.jpg)

* Connect Rx pin of device1 to Tx pin of device2 ,similarly Tx pin of device1 to Rx pin of device2.
* If the device1 works on 5v and device2 works at 3.3v then use the level shifting mechanism(voltage divider )
* Genrally UART is used to communicate with board through USB to TTL connection .
* USB to TTL connection does not require a protection circuit .
* Whereas Senor interfacing using UART might require a protection circuit.

2. I2C  
I2C is a synchronous, multi-master, multi-slave, packet switched, single-ended, serial communication bus. It is widely used for attaching lower-speed peripheral ICs to processors and microcontrollers in short-distance, intra-board communication.

![i2c](https://cdn.sparkfun.com/assets/3/d/1/b/6/51adfda8ce395f151b000000.png)

* While using I2c interfaces with sensors SDA and SDL lines must be protected.
* Protection of these lines is done by using pullup registers on both lines.
* If you use the inbuilt pullup registers in the board you wont need an external circuit.
* If you are using bread-board to connect your sensor , use the pullup resistor .
* Generally, 2.2kohm <= 4K ohm resistors are used.

3. SPI  
SPI (Serial Peripheral Interface) is a full duplex synchronous serial communication interface used for short distance communications. It is usually used for communication between different modules in a same device or PCB. SPI devices communicates each other using a master slave architecture with a single master. Multiple slaves are supported through individual slave select lines.

![spi](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/SPI_three_slaves.svg/350px-SPI_three_slaves.svg.png)

* Generally ,Spi in development boards is in Push-pull mode.
* Push-pull mode does not require any protection circuit.
* On SPI interface if you are using more than one slaves it is possible that the device2 can "hear" and "respond" to the master's communication with device1- which is an disturbance .
* To overcome this problem , we use a protection circuit with pullup resistors on each the Slave Select line(CS).
* Resistors value can be between 1kOhm ~10kOhm . Generally 4.7kOhm resistor is used.